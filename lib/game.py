import pygame
from lib.snake import Snake
import time
import random
import os
import tkinter as tk

class Game:

    def __init__(self):
        #current directory to get files
        currentDir = os.getcwd()

        #initialise pygame
        pygame.init()

        #Sound
        music_path = os.path.join(currentDir, 'music/Marimba_Boy.wav')
        pygame.mixer.music.load(music_path)
        pygame.mixer.music.play(-1)

        bite_sound_path = os.path.join(currentDir, "music/bite.mp3")
        self.bite_sound = pygame.mixer.Sound(bite_sound_path)

        #Points
        self.points = 0

        #Game zone
        self.nbr_case_X = 20
        self.nbr_case_Y = 20
        self.total_cases = self.nbr_case_X*self.nbr_case_Y
        self.case_size = 32

        self.left=50 #Sides border
        self.top=50 #Top and bottom border
        self.width = (self.nbr_case_X) * self.case_size
        self.height = (self.nbr_case_Y) * self.case_size
        self.filled=0

        #Create Snake
        self.snake = Snake(8,10)
        self.snake_player_image = self.snake.snake_player_image

        #Food
        self.food = []
        self.place_food(5)
        food_image_path = os.path.join(currentDir, "image/banana_32.png")
        self.food_icon = pygame.image.load(food_image_path)

        eaten_food_image_path =  os.path.join(currentDir, "image/banana_peel_32.png")
        self.eaten_food_icon = pygame.image.load(eaten_food_image_path)

        #Create screen
        self.screen_width = self.width+(2*self.left)
        self.screen_heigth = self.height+(2*self.top)
        self.screen = pygame.display.set_mode((self.screen_width, self.screen_heigth))

        #Title and icon
        pygame.display.set_caption("Monkey")

        icon_path = os.path.join(currentDir, "image/monkey_icon.png")
        icon = pygame.image.load(icon_path)
        pygame.display.set_icon(icon)
    
    def start_game(self):
        self.running = True
        snake_alive = True
        while self.running:
            snake_moved = False
            self.screen.fill((0,102,0))
            self.update_points()

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.running = False

                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_RIGHT:
                        self.valid_move("R")
                        self.snake.move_right()
                        self.eat_food()
                    if event.key == pygame.K_LEFT:
                        self.valid_move("L")
                        self.snake.move_left()
                        self.eat_food()
                    if event.key == pygame.K_UP:
                        self.valid_move("U")
                        self.snake.move_up()
                        self.eat_food()
                    if event.key == pygame.K_DOWN:
                        self.valid_move("D")
                        self.snake.move_down()
                        self.eat_food()
                    snake_moved = True
            
            if not snake_moved:
                self.snake.keep_moving()
                self.eat_food()

            if self.snake_dead() or self.win_game():
                self.running = False
                self.game_over()

            pygame.draw.rect(self.screen,[153,255,153], [self.left, self.top, self.width, self.height], self.filled)
            pygame.display.flip()
            self.draw_food()
            self.draw_snake()
            pygame.display.update()

            time.sleep(0.3)

    def valid_move(self, move):
        #Monkey cant go back
        if move == "R" and self.snake.direction == "L":
            self.running = False
        if move == "L" and self.snake.direction == "R":
           self.running = False
        if move == "U" and self.snake.direction == "D":
            self.running = False
        if move == "D" and self.snake.direction == "U":
           self.running = False

    def snake_dead(self):

        if self.snake.snake_head_position[0] < 0 or self.snake.snake_head_position[0] > (self.nbr_case_X-1):
            return True
        if self.snake.snake_head_position[1] < 0 or self.snake.snake_head_position[1] > (self.nbr_case_Y-1):
            return True
        
        if self.snake.snake_head_position in self.snake.body_part[1:]:
            return True
            
        return False
    
    def win_game(self):
        if len(self.snake.body_part) == (self.total_cases):
            print("WIN!!")
            return True
        return False


    def draw_snake(self):

        i=0
        for bodyPart in self.snake.body_part:

            left = (bodyPart[0]*self.case_size) + self.left
            top = (((self.nbr_case_Y - bodyPart[1])*self.case_size)+self.top-self.case_size)
            
            if i == 0:
                self.screen.blit(self.snake_player_image, (left, top))
            else:
                self.screen.blit(self.eaten_food_icon, (left, top))
            i += 1

    def place_food(self, number_of_piece):

        for i in range(number_of_piece):

            if (len(self.snake.body_part) + len(self.food)) < (self.total_cases):     
                random_position = [random.randint(0,self.nbr_case_X-1),random.randint(0,self.nbr_case_Y-1)]

                while random_position in self.snake.body_part or random_position in self.food:
                    random_position = [random.randint(0,self.nbr_case_X-1),random.randint(0,self.nbr_case_Y-1)]

                self.food.append(random_position)
    
    def draw_food(self):
        for food in self.food:
            left = (food[0]*self.case_size) + self.left
            top = (((self.nbr_case_Y - food[1])*self.case_size)+self.top-self.case_size)
            
            self.screen.blit(self.food_icon, (left, top))

    def eat_food(self):
        for food in self.food:
            if self.snake.snake_head_position == food:
                self.snake.eat_food()
                self.food.remove(food)
                self.place_food(1)
                self.points += 10
                pygame.mixer.Sound.play(self.bite_sound)

    
    def update_points(self):
        font = pygame.font.Font('freesansbold.ttf', 16)
        text = font.render('Score: ' + str(self.points), True, (0,102,0), (153,255,153))
        textRect = text.get_rect()
        textRect.bottomleft = (50, 35)
        self.screen.blit(text, textRect)

    def game_over(self, winner=True):

        self.gameOverWindow = tk.Tk()
        self.gameOverWindow.geometry("300x300")

        totalPoints = tk.Label(self.gameOverWindow, text="Score = " + str(self.points))
        totalPoints.pack()

        #Play Again button
        playAgainButton = tk.Button(self.gameOverWindow, text="Play again", command=self.play_again)
        playAgainButton.pack()

        #Quit button
        quitButton = tk.Button(self.gameOverWindow, text="Quit", command=self.quit)
        quitButton.pack()

        self.gameOverWindow.mainloop()

    def play_again(self):
        pygame.quit()
        self.gameOverWindow.destroy()
        Game().start_game()
    
    def quit(self):
        pygame.quit()
        self.gameOverWindow.destroy()

        

