import os
import pygame

class Snake:
    def __init__(self, positionX, positionY):
        self.last_tail_position = [positionX, positionY]
        self.last_head_position = [positionX, positionY]
        self.snake_head_position = [positionX, positionY]
        self.body_part = [[positionX, positionY]]
        self.direction = "R"

        #Snake icon
        currentDir = os.getcwd()
        snake_image_path = os.path.join(currentDir, "image/monkey_32.png")
        self.snake_player_image = pygame.image.load(snake_image_path)
    
    def move_right(self):
        self.direction = "R"
        self.last_tail_position = self.body_part[-1]
        position_to_move = self.body_part[0]
        i=0
        for bodyPart in self.body_part:
            if i == 0:
                self.body_part[0] = [bodyPart[0]+1, bodyPart[1]]
                i += 1
            else:
                position_temp = bodyPart
                self.body_part[i] = position_to_move
                position_to_move = position_temp
                i += 1
        self.update_snake_head_position()

    def move_left(self):
        self.direction = "L"
        self.last_tail_position = self.body_part[-1]
        position_to_move = self.body_part[0]
        i=0
        for bodyPart in self.body_part:
            if i == 0:
                self.body_part[0] = [bodyPart[0]-1, bodyPart[1]]
                i += 1
            else:
                position_temp = bodyPart
                self.body_part[i] = position_to_move
                position_to_move = position_temp
                i += 1
        self.update_snake_head_position()
        
    
    def move_up(self):
        self.direction = "U"
        self.last_tail_position = self.body_part[-1]
        position_to_move = self.body_part[0]
        i=0
        for bodyPart in self.body_part:
            if i == 0:
                self.body_part[0] = [bodyPart[0], bodyPart[1]+1]
                i += 1
            else:
                position_temp = bodyPart
                self.body_part[i] = position_to_move
                position_to_move = position_temp
                i += 1
        self.update_snake_head_position()

    def move_down(self):
        self.direction = "D"
        self.last_tail_position = self.body_part[-1]
        position_to_move = self.body_part[0]
        i=0
        for bodyPart in self.body_part:
            if i == 0:
                self.body_part[0] = [bodyPart[0], bodyPart[1]-1]
                i += 1
            else:
                position_temp = bodyPart
                self.body_part[i] = position_to_move
                position_to_move = position_temp
                i += 1
        self.update_snake_head_position()

    def eat_food(self):
        self.body_part.append(self.last_tail_position)

    def show_snake(self):
        for piece in self.body_part:
            print(piece)

    def update_snake_head_position(self):
        self.snake_head_position = self.body_part[0]

    def keep_moving(self):
        if self.direction == "R":
            self.move_right()
        if self.direction == "L":
            self.move_left()
        if self.direction == "U":
            self.move_up()
        if self.direction == "D":
            self.move_down()
if __name__ == "__main__":
    
    snake = Snake(2,2)
    snake.body_part = [[3,2], [2,2]]

    snake.move_right()
    snake.eat_food()
    snake.show_snake()






