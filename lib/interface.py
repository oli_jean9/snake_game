from tkinter import NSEW, NW, W, EW, Canvas, Label, Tk, Button, END, Frame, Listbox, Toplevel, Entry, messagebox, DISABLED
from PIL import Image, ImageTk
import tkinter.font as TkFont
import pygame
from lib.game import Game
import os

class Menu(Canvas):
    """Classe héritant d'un Canvas, et qui affichera les élements de l'écran d'acceuil.

    """

    def __init__(self):
        super().__init__()


        currentDir = os.getcwd()

        #Création du Canvas
        width = 1080
        height = 720
        self.menu_canvas = Canvas(width=width, height=height, bg='black')
        self.menu_canvas.pack()

        # Image de fond
        background_image_path = os.path.join(currentDir, 'image/menu_monkey.jpg')
        self.background_image = Image.open(background_image_path)
        self.background_image_copy = self.background_image.copy()

        self.photo = ImageTk.PhotoImage(self.background_image)
        self.menu_canvas.create_image(0,0,image=self.photo,anchor=NW)

        # Button
        self.jouer_button = Button(self.menu_canvas, text="Start game")
        self.jouer_button.w = self.menu_canvas.create_window(width/2,height*0.85,window=self.jouer_button)

class MasterMenu(Tk):
    """Classe héritant d'une fenêtre Tk, à laquelle sera intégré un objet de la classe Menu.

    """

    def __init__(self):
        super().__init__()

        currentDir = os.getcwd()

        # Paramètre de dimensionnement de la fenêtre
        self.pack_propagate(0)
        self.config(width=1080, height=720)
        self.resizable(False, False)
        self.title("Monkey")

        monkey_icon_path = os.path.join(currentDir, "image/monkey_icon.png")
        photo = ImageTk.PhotoImage(file = monkey_icon_path)
        self.iconphoto(False, photo)

        # Création d'un canvas Menu
        canvas_menu = Menu()

        # Définition de la commande des boutton du canvas
        canvas_menu.jouer_button.config(command=self.start_game)

        # Intégration d'un canvas Menu à la fenêtre
        canvas_menu.pack()

    def start_game(self):
        """Méthode qui permet de commencer une partie.

        """
        self.destroy()
        Game().start_game()

